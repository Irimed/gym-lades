import pygame
import pygame_gui
import random

pygame.init()


screen_width = 1280
screen_height = 720
screen = pygame.display.set_mode((screen_width, screen_height))

text_font = pygame.font.SysFont("Arial", 30)
text_font2 = pygame.font.SysFont("Arial", 60)

Manager = pygame_gui.UIManager((screen_width, screen_height))
Clock = pygame.time.Clock()


ENTREE_BOUTON = pygame.image.load('ENTREE.png').convert_alpha()
QUITTER_BOUTON = pygame.image.load('quitter_temp.png').convert_alpha()
ENTREZ_IMC_BOUTON = pygame.image.load('entrez_temp.png').convert_alpha()
RETOUR_BOUTON = pygame.image.load('RETOUR.png').convert_alpha()
SEANCE_BOUTON = pygame.image.load('seance_temp.png').convert_alpha()
CALCUL_BOUTON = pygame.image.load('calcul_temp.png').convert_alpha()
HAUT_DU_CORPS_BOUTON = pygame.image.load('exos\\haut_du_corps.png').convert_alpha()
BAS_DU_CORPS_BOUTON = pygame.image.load('exos\\bas_du_corps.png').convert_alpha()
ADDUCTION_IMAGE = pygame.image.load('exos\\adduction.png').convert_alpha()
BULGARIE_SQUAT_IMAGE = pygame.image.load('exos\\bulgarie_squat.png').convert_alpha()
BUTTERFLY_IMAGE = pygame.image.load('exos\\butterfly.png').convert_alpha()
CEINTURE_ABS_IMAGE = pygame.image.load('exos\\ceinture_abs.png').convert_alpha()
DEADLIFT_IMAGE = pygame.image.load('exos\\deadlift.png').convert_alpha()
DEV_COUCHER_IMAGE = pygame.image.load('exos\\dev_coucher.png').convert_alpha()
DEV_MILITAIRE_IMAGE = pygame.image.load('exos\\dev_militaire.png').convert_alpha()
ELEVATION_LATERALE_IMAGE = pygame.image.load('exos\\élévation_latérale.png').convert_alpha()
HIP_TRUST_IMAGE = pygame.image.load('exos\\hip_trust.png').convert_alpha()
KICKBACK_IMAGE = pygame.image.load('exos\\kickback.png').convert_alpha()
LEG_CURL_IMAGE = pygame.image.load('exos\\leg_curl.png').convert_alpha()
LEG_EXTENSION_IMAGE = pygame.image.load('exos\\leg_extension.png').convert_alpha()
LEG_PRESS_IMAGE = pygame.image.load('exos\\leg_press.png').convert_alpha()
RDL_IMAGE = pygame.image.load('exos\\RDL.png').convert_alpha()
SQUAT_IMAGE = pygame.image.load('exos\\squat.png').convert_alpha()
TIRAGE_HORIZONTALE_IMAGE = pygame.image.load('exos\\tirage_horizontale.png').convert_alpha()

list_exo_haut = [BUTTERFLY_IMAGE, CEINTURE_ABS_IMAGE, DEV_COUCHER_IMAGE, DEV_MILITAIRE_IMAGE, ELEVATION_LATERALE_IMAGE,TIRAGE_HORIZONTALE_IMAGE ]
list_exo_bas = [ADDUCTION_IMAGE, BULGARIE_SQUAT_IMAGE, DEADLIFT_IMAGE, KICKBACK_IMAGE, LEG_CURL_IMAGE, LEG_EXTENSION_IMAGE, LEG_PRESS_IMAGE, RDL_IMAGE,SQUAT_IMAGE, HIP_TRUST_IMAGE]

class Button():
    def __init__(self, x, y, image, scale):
        self.image = pygame.transform.scale(image, (int(image.get_width()*scale), int(image.get_height()*scale)))
        self.rect = self.image.get_rect()
        self.rect.topleft = (x,y)
        self.clicked = False

    def draw(self):

        pos = pygame.mouse.get_pos()
        if self.rect.collidepoint(pos):
            if pygame.mouse.get_pressed()[0] == 1:
                self.clicked = True
                print("cliked")

        screen.blit(self.image, (self.rect.x, self.rect.y))

    def checkForInput(self, position):
        if position[0] in range(self.rect.left, self.rect.right) and position[1] in range(self.rect.top, self.rect.bottom) :
            return True
        return False


def draw_text(text, font, text_col, x, y):
    img = font.render(text, True, text_col)
    screen.blit(img, (x, y))

background = pygame.image.load('bg.png')

def calcul_imc(kg,t):
    etat=""
    IMC = kg / (t/100)**2# IMC = poids en kg / taille²
    if IMC>=40:
        etat="obesité morbide"
    elif IMC<40 and IMC>35:
        etat="obesité sévère "
    elif IMC<35 and IMC>30:
        etat="obesité modérée"
    elif IMC>30 and IMC<25:
        etat="surpoids"
    elif IMC>25 and IMC<18.5:
        etat="corpulence normale"
    elif IMC>18.5 and IMC<16.5:
        etat="maigreur"
    elif IMC<16.5:
        etat="maigreur extrême"
    return f"Vous avez {IMC} vous êtes donc en {etat}"

def menu():
    global running, pos
    running = True
    pygame.display.set_caption("Menu")
    screen.fill("black")
    start_button = Button(screen_width/2 - ENTREE_BOUTON.get_width()/2-200, screen_height/2 - ENTREE_BOUTON.get_height()/2, ENTREE_BOUTON, 0.75)
    quit_button = Button(screen_width/2 - QUITTER_BOUTON.get_width()/2+350, screen_height/2 - QUITTER_BOUTON.get_height()/2, QUITTER_BOUTON, 0.75)
    while running:
        screen.blit(background, (0,0))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                print("Fermeture de la fenêtre")


            if event.type == pygame.MOUSEBUTTONDOWN :
                if start_button.checkForInput(pygame.mouse.get_pos()):
                    chui_entre()
                if quit_button.checkForInput(pygame.mouse.get_pos()):
                    quit()


        #screen.blit(pygame.image.load('amin_uwu.png'), (0, 0))
        #draw_text("Hello World", text_font, (0, 0, 0), 720/2-30, 480/2-10)
        start_button.draw()
        quit_button.draw()
        pygame.display.flip()

def chui_entre():
    global running, pos
    running = True   
    pygame.display.set_caption("CHUI ENTREE")
    screen.fill("black") 
    seance_button = Button(screen_width/2 - SEANCE_BOUTON.get_width()/2-200, screen_height/2 - SEANCE_BOUTON.get_height()/2, SEANCE_BOUTON, 0.75)
    calcul_imc_button = Button(screen_width/2 - CALCUL_BOUTON.get_width()/2+350, screen_height/2 - CALCUL_BOUTON.get_height()/2, CALCUL_BOUTON, 0.75)
    return_button = Button(screen_width/2 - RETOUR_BOUTON.get_width()/2 -190, screen_height/2 - RETOUR_BOUTON.get_height()/2- 175, RETOUR_BOUTON, 0.25)
    while running :
        screen.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                print("Fermeture de la fenêtre")
            if event.type == pygame.MOUSEBUTTONDOWN :
                    if seance_button.checkForInput(pygame.mouse.get_pos()):
                        choix_partie()
                    if calcul_imc_button.checkForInput(pygame.mouse.get_pos()):
                        calcul_imc()
                    if return_button.checkForInput(pygame.mouse.get_pos()):
                        menu()
        
        seance_button.draw()
        calcul_imc_button.draw()
        return_button.draw()

        pygame.display.flip()

def calcul_imc():
    global running
    running = True
    screen.fill("black")
    pygame.display.set_caption("CHUI ENTREE")
    UI_REFRESH_RATE = Clock.tick(60)/1000
    poids_input = pygame_gui.elements.UITextEntryLine(relative_rect=pygame.Rect((350, 275), (400, 50)), manager=Manager, object_id="#poids")
    taille_input = pygame_gui.elements.UITextEntryLine(relative_rect=pygame.Rect((350, 375), (400, 50)), manager=Manager, object_id="#taille")
    entrez_imc_button = Button(screen_width/2 - ENTREZ_IMC_BOUTON.get_width()/2, screen_height/2 - ENTREZ_IMC_BOUTON.get_height()/2 + 300, ENTREZ_IMC_BOUTON, 0.75)
    return_button = Button(screen_width/2 - RETOUR_BOUTON.get_width()/2 -190, screen_height/2 - RETOUR_BOUTON.get_height()/2- 175, RETOUR_BOUTON, 0.25)
    
    poids = 0
    taille = 0

    while running:
        screen.blit(background, (0,0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                print("Fermeture de la fenêtre")
            if event.type == pygame.USEREVENT:  # Vérifier les événements utilisateur pygame_gui
                if event.type == pygame_gui.UI_TEXT_ENTRY_FINISHED:
                    if event.ui_object_id == "#poids":
                        poids = int(poids_input.text)
                    elif event.ui_object_id == "#taille":
                        taille = int(taille_input.text)
            if event.type == pygame.MOUSEBUTTONDOWN :
                if entrez_imc_button.checkForInput(pygame.mouse.get_pos()):
                    return poids, taille  # Retourne les valeurs de poids et taille
                if return_button.checkForInput(pygame.mouse.get_pos()):
                    menu()
            Manager.process_events(event)

        Manager.update(UI_REFRESH_RATE)
        Manager.draw_ui(screen)
        
        entrez_imc_button.draw()
        return_button.draw()
        draw_text("Calculez votre imc :", text_font, "white", screen_width/2 - ENTREE_BOUTON.get_width()/2,screen_height/2 - ENTREE_BOUTON.get_height()/2)
        draw_text("Taille (en cm):", text_font, "white",190, 375)
        draw_text("Poids (en kg):", text_font, "white",190, 275)
        draw_text("Actuellement indisponible",text_font2,"white",190, 155)
        pygame.display.flip()


def show_imc():
    global running
    running = True
    screen.fill("black")
    pygame.display.set_caption("IMC")

    poids, taille = chui_entre()  # Récupère les valeurs de poids et taille

    new_text = text_font.render(calcul_imc(poids, taille), True, "white")
    new_text_rect = new_text.get_rect(center=(screen_width/2, screen_height/2))
    screen.blit(new_text, new_text_rect)

    Clock.tick(60)

    while running:
        screen.blit(background, (0,0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                print("Fermeture de la fenêtre")

        draw_text("Calculez votre imc :", text_font, "white", screen_width/2 - ENTREE_BOUTON.get_width()/2, screen_height/2 - ENTREE_BOUTON.get_height()/2)
        pygame.display.flip()

    while running:
        screen.blit(background, (0,0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                print("Fermeture de la fenêtre")

        #screen.blit(pygame.image.load('amin_uwu.png'), (0, 0))
        draw_text("Calculez votre imc :", text_font, "white", screen_width/2 - ENTREE_BOUTON.get_width()/2,screen_height/2 - ENTREE_BOUTON.get_height()/2)
        pygame.display.flip()

def choix_partie(): 
    global running
    running = True
    screen.fill("black")
    pygame.display.set_caption("Liste d'exercices")
    return_button = Button(screen_width/2 - RETOUR_BOUTON.get_width()/2 -190, screen_height/2 - RETOUR_BOUTON.get_height()/2- 175, RETOUR_BOUTON, 0.25)
    haut_du_corps_button = Button(screen_width/2 - HAUT_DU_CORPS_BOUTON.get_width()/2-200, screen_height/2 - HAUT_DU_CORPS_BOUTON.get_height()/2, HAUT_DU_CORPS_BOUTON, 0.75)
    bas_du_corps_button = Button(screen_width/2 - BAS_DU_CORPS_BOUTON.get_width()/2+350, screen_height/2 - BAS_DU_CORPS_BOUTON.get_height()/2, BAS_DU_CORPS_BOUTON, 0.75)

    while running :
        screen.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                print("Fermeture de la fenêtre")
            if event.type == pygame.MOUSEBUTTONDOWN :
                    if return_button.checkForInput(pygame.mouse.get_pos()):
                        chui_entre()
                    if haut_du_corps_button.checkForInput(pygame.mouse.get_pos()):
                        haut_du_corps()
                    if bas_du_corps_button.checkForInput(pygame.mouse.get_pos()):
                        bas_du_corps()
        return_button.draw()
        haut_du_corps_button.draw()
        bas_du_corps_button.draw()
        pygame.display.flip()

def haut_du_corps():
    global running
    running = True
    screen.fill("black")
    pygame.display.set_caption("Haut du corps")
    return_button = Button(screen_width/2 - RETOUR_BOUTON.get_width()/2 -190, screen_height/2 - RETOUR_BOUTON.get_height()/2- 175, RETOUR_BOUTON, 0.25)
    exo1_image = random.choice(list_exo_haut)
    exo2_image = random.choice(list_exo_haut)
    exo3_image = random.choice(list_exo_haut)
    exo1_button = Button(screen_width/2 - exo1_image.get_width()/2-112, screen_height/2 - exo1_image.get_height()/2, exo1_image, 0.55)
    exo2_button = Button(screen_width/2 - exo2_image.get_width()/2+350, screen_height/2 - exo2_image.get_height()/2, exo2_image, 0.55)
    exo3_button = Button(screen_width/2 - exo3_image.get_width()/2+120, screen_height/2 - exo3_image.get_height()/2 + 200, exo3_image, 0.55)

    while running :
        screen.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                print("Fermeture de la fenêtre")
            if event.type == pygame.MOUSEBUTTONDOWN :
                    if return_button.checkForInput(pygame.mouse.get_pos()):
                        choix_partie()
        return_button.draw()
        exo1_button.draw()
        exo2_button.draw()  
        exo3_button.draw() 
        pygame.display.flip()

def bas_du_corps():
    global running
    running = True
    screen.fill("black")
    pygame.display.set_caption("Bas du corps")
    return_button = Button(screen_width/2 - RETOUR_BOUTON.get_width()/2 -190, screen_height/2 - RETOUR_BOUTON.get_height()/2- 175, RETOUR_BOUTON, 0.25)
    exo1_image = random.choice(list_exo_bas)
    exo2_image = random.choice(list_exo_bas)
    exo3_image = random.choice(list_exo_bas)
    exo1_button = Button(screen_width/2 - exo1_image.get_width()/2-112, screen_height/2 - exo1_image.get_height()/2, exo1_image, 0.55)
    exo2_button = Button(screen_width/2 - exo2_image.get_width()/2+350, screen_height/2 - exo2_image.get_height()/2, exo2_image, 0.55)
    exo3_button = Button(screen_width/2 - exo3_image.get_width()/2+120, screen_height/2 - exo3_image.get_height()/2 + 200, exo3_image, 0.55)

    while running :
        screen.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                print("Fermeture de la fenêtre")
            if event.type == pygame.MOUSEBUTTONDOWN :
                    if return_button.checkForInput(pygame.mouse.get_pos()):
                        choix_partie()
        return_button.draw()
        exo1_button.draw()
        exo2_button.draw()  
        exo3_button.draw()
        pygame.display.flip()

menu()


